package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyDataBeans implements Serializable {

	private int id;
	private int userId;
	private int price;
	private int prefectures_id;
	private String yoyaku_date;
	private String yoyaku_time;
	private String usercomment;
	private Date buyDate;

	private String prefecturesName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getPrefectures_id() {
		return prefectures_id;
	}

	public void setPrefectures_id(int prefectures_id) {
		this.prefectures_id = prefectures_id;
	}

	public String getYoyaku_date() {
		return yoyaku_date;
	}

	public void setYoyaku_date(String yoyaku_date) {
		this.yoyaku_date = yoyaku_date ;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);

	}

	public String getPrefecturesName() {
		return prefecturesName;
	}

	public void setPrefecturesName(String prefecturesName) {
		this.prefecturesName = prefecturesName;
	}

	public String getYoyaku_time() {
		return yoyaku_time;
	}

	public void setYoyaku_time(String yoyaku_time) {
		this.yoyaku_time = yoyaku_time;
	}

	public String getUsercomment() {
		return usercomment;
	}

	public void setUsercomment(String usercomment) {
		this.usercomment = usercomment;
	}
}
