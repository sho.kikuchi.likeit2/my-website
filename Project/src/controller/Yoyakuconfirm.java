package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ShopDataBeans;
import dao.ShopDao;


/**
 * Servlet implementation class Yoyakuconfirm
 */
@WebServlet("/YoyakuConfirm")
public class Yoyakuconfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			int id = Integer.parseInt(request.getParameter("item_id"));
			ShopDataBeans item = ShopDao.getItemByItemID(id);
			String inputUserYoyakuDate = request.getParameter("yoyaku.date");
			String inputUserYoyakutime = request.getParameter("yoyaku.time");
			String yoyskuUserComment = request.getParameter("yoyaku.usercomment");
			//Beansを取得
			BuyDataBeans bdb = new BuyDataBeans();
			bdb.setUserId((int) session.getAttribute("userId"));
			bdb.setPrice(item.getPrice());
			bdb.setUsercomment(yoyskuUserComment);
			bdb.setYoyaku_date(inputUserYoyakuDate);
			bdb.setYoyaku_time(inputUserYoyakutime);


			//購入確定で利用
			session.setAttribute("bdb", bdb);
			session.setAttribute("item", item);
			request.getRequestDispatcher(EcHelper.YOYAKU_CONFIRM_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
