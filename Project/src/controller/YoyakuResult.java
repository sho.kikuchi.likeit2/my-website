package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ShopDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;
import dao.ShopDao;

/**
 * Servlet implementation class YoyakuResult
 */
@WebServlet("/YoyakuResult")
public class YoyakuResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
	try {

		BuyDataBeans bdb = (BuyDataBeans) session.getAttribute("bdb");
		int id = Integer.parseInt(request.getParameter("item_id"));
		ShopDataBeans item = ShopDao.getItemByItemID(id);
		int yoyakuId = BuyDao.insertBuy(bdb);

		BuyDetailDataBeans bddb = new BuyDetailDataBeans();
		bddb.setBuyId(yoyakuId);
		bddb.setItemId(item.getId());
		BuyDetailDao.insertBuyDetail(bddb);

		/* ====購入完了ページ表示用==== */
		BuyDataBeans resultBDB = BuyDao.getBuyDataBeansByBuyId(yoyakuId);
		request.setAttribute("resultBDB", resultBDB);
		// 購入アイテム情報
		ShopDataBeans buyIDBList = BuyDetailDao.getItemDataBeansListByBuyId(yoyakuId);
		request.setAttribute("buyIDBList", buyIDBList);


		request.getRequestDispatcher(EcHelper.YOYAKU_RESULT_PAGE).forward(request, response);
		removeAttribute("bdb");
	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
}


	private void removeAttribute(String string) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
