package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShopDataBeans;
import dao.ShopDao;

/**
 * Servlet implementation class ShopDeleteResult
 */
@WebServlet("/ShopDeleteResult")
public class ShopDeleteResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

			int id = Integer.parseInt(request.getParameter("shop_id"));
			List<ShopDataBeans> shoplist = ShopDao.GetfindAll();

			ShopDao.DeleteShopData(id);
			request.setAttribute("shoplist", shoplist);
			request.getRequestDispatcher(EcHelper.SHOP_DELETE_RESULT_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}