package controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;

public class EcHelper {

	public static final String TOP_PAGE = "/WEB-INF/jsp/index.jsp";

	public static final String REGIST_PAGE = "/WEB-INF/jsp/regist.jsp";

	public static final String REGIST_CONFIRM_PAGE = "/WEB-INF/jsp/registconfirm.jsp";

	public static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";

	public static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/itemserchresult.jsp";

	public static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";

	public static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";

	public static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/registresult.jsp";

	public static final String USER_DATA_PAGE = "/WEB-INF/jsp/userdata.jsp";

	public static final String USER_DATA_UPDATE_CONFIRM_PAGE = "/WEB-INF/jsp/userdataupdateconfirm.jsp";

	public static final String USER_DATA_UPDATA_RESULT_PAGE = "/WEB-INF/jsp/userdataupdateresult.jsp";

	public static final String USER_YOYAKU_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/useryoyakuhistorydetail.jsp";

	public static final String ITEM_PAGE = "/WEB-INF/jsp/item.jsp";

	public static final String YOYAKU_PAGE = "/WEB-INF/jsp/yoyaku.jsp";

	public static final String YOYAKU_CONFIRM_PAGE = "/WEB-INF/jsp/yoyakuconfirm.jsp";

	public static final String YOYAKU_RESULT_PAGE = "/WEB-INF/jsp/yoyakuresult.jsp";

	public static final String SHOP_REGIST_PAGE = "/WEB-INF/jsp/shopregist.jsp";

	public static final String SHOP_REGIST_RESULT_PAGE = "/WEB-INF/jsp/shopregistresult.jsp";

	public static final String SHOP_LIST = "/WEB-INF/jsp/shoplist.jsp";

	public static final String SHOP_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/shophistorydetail.jsp";

	public static final String SHOP_HISTORY_RESULT_PAGE = "/WEB-INF/jsp/shophistoryresult.jsp";

	public static final String SHOP_DELETE_RESULT_PAGE = "/WEB-INF/jsp/shopdeleterusult.jsp";

	public static final String SHOP_DELETE_PAGE = "/WEB-INF/jsp/shopdelete.jsp";

	public static final String USER_LIST = "/WEB-INF/jsp/userlist.jsp";

	public static final String USER_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/userhistorydetail.jsp";

	public static final String USER_HISTORY_RESULT_PAGE = "/WEB-INF/jsp/userhistoryresult.jsp";

	public static final String USER_DELETE_PAGE = "/WEB-INF/jsp/userdelete.jsp";

	public static final String USER_DELETE_RESULT_PAGE = "/WEB-INF/jsp/userdeleteresult.jsp";

	public static EcHelper getInstance() {

		return new EcHelper();
	}

	/**
	 *
	 * ハッシュ関数
	 *
	 * @param target
	 * @return
	 */
	public static String getSha256(String target) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(target.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	 * ログインIDのバリデーション
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		return false;

	}

}
