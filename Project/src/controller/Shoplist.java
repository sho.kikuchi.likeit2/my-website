package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShopDataBeans;
import dao.ShopDao;

/**
 * Servlet implementation class Shoplist
 */
@WebServlet("/Shoplist")
public class Shoplist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;

			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnStrUrl", "Index");
				// Login画面にリダイレクト
				response.sendRedirect("Login");
			} else {
			List<ShopDataBeans> shoplist = ShopDao.GetfindAll();

			request.setAttribute("shoplist", shoplist);
			request.getRequestDispatcher(EcHelper.SHOP_LIST).forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		try {

			int ShopId = Integer.parseInt(request.getParameter("Shop_id"));
			List<ShopDataBeans> Shopsearch = ShopDao.GetShopSearch(ShopId);
			request.setAttribute("shoplist", Shopsearch);
			request.getRequestDispatcher(EcHelper.SHOP_LIST).forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
