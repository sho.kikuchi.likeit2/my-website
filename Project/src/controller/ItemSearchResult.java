package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PrefecturesDataBeans;
import beans.ShopDataBeans;
import beans.UserDataBeans;
import dao.PrefecturesDao;
import dao.ShopDao;

/**
 * Servlet implementation class item
 */
@WebServlet("/ItemSearchResult")
public class ItemSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static int PAGE_MAX_ITEM_COUNT = 8;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			int PrefecturesId = Integer.parseInt(request.getParameter("prefectures_id"));
			String searchWord = request.getParameter("search_word");
			ArrayList<PrefecturesDataBeans> pdList = PrefecturesDao.getAllPrefecturesDataBeans();

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("searchWord", searchWord);
			// 商品リストを取得 ページ表示分のみ
			ArrayList<ShopDataBeans> searchResultItemList = ShopDao.getShopByItemName(searchWord, PrefecturesId,
					pageNum, PAGE_MAX_ITEM_COUNT);
			// 検索ワードに対しての総ページ数を取得
			double ShopCount = ShopDao.getShopCount(searchWord, PrefecturesId);
			int pageMax = (int) Math.ceil(ShopCount / PAGE_MAX_ITEM_COUNT);
			UserDataBeans udb = new UserDataBeans();
			udb.setPrefectures_id(PrefecturesId);
			session.setAttribute("pdlist", pdList);

			session.setAttribute("udb", udb);

			session.setAttribute("searchWord", searchWord);
			//総アイテム数
			request.setAttribute("itemCount", (int) ShopCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", searchResultItemList);

			request.getRequestDispatcher(EcHelper.SEARCH_RESULT_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
