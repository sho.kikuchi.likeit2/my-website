package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PrefecturesDataBeans;
import beans.UserDataBeans;
import dao.PrefecturesDao;
import dao.UserDao;

/**
 * Servlet implementation class UserhistoryDetail
 */
@WebServlet("/UserhistoryDetail")
public class UserhistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
					: false;

			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnStrUrl", "Index");
				// Login画面にリダイレクト
				response.sendRedirect("Login");
			} else {
			ArrayList<PrefecturesDataBeans> pdList = PrefecturesDao.getAllPrefecturesDataBeans();
			String id = request.getParameter("user_id");
			int UserId = Integer.parseInt(id);
			UserDataBeans Userlist = UserDao.GetUserSearchId(UserId);
			request.setAttribute("userlist", Userlist);
			session.setAttribute("pdlist", pdList);
			request.getRequestDispatcher(EcHelper.USER_HISTORY_DETAIL_PAGE).forward(request, response);
			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		try {
			int prefectures_id = Integer.parseInt(request.getParameter("prefectures_id"));
			int userId = Integer.parseInt(request.getParameter("user_id"));
			// 入力フォームから受け取った値をUserDataBeansにセット
			UserDataBeans udb = new UserDataBeans();
			udb.setUpdateUserDataBeansInfo(request.getParameter("user_name"), request.getParameter("login_id"), request.getParameter("user_address"),
					userId, prefectures_id);

			// アップデート処理
				UserDao.updateUser(udb);
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(EcHelper.USER_DATA_UPDATA_RESULT_PAGE).forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
		}
	}

