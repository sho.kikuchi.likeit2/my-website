package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ShopDataBeans;
import beans.UserDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;
import dao.UserDao;

@WebServlet("/UseryoyakuHistoryDetail")
public class UseryoyakuhistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			int userId = (int) session.getAttribute("userId");
			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDao.getUserDataBeansByUserId(userId)
					: (UserDataBeans) EcHelper.cutSessionAttribute(session, "returnUDB");
			request.setAttribute("udb", udb);
			String id = request.getParameter("buy_id");
			int BuyId = Integer.parseInt(id);
			BuyDataBeans buylist = BuyDao.getBuyDataBeansByBuyId(BuyId);
			request.setAttribute("buylist", buylist);
			//ArrayList<ShopDataBeans> buyI = BuyDetailDao.getItemDataBeansListByBuyId(BuyId);
			ShopDataBeans buyI = BuyDetailDao.getItemDataBeansListByBuyId(BuyId);
			request.setAttribute("buyI", buyI);
			// Servletのデータ受け取り
			request.getRequestDispatcher(EcHelper.USER_YOYAKU_HISTORY_DETAIL_PAGE).forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
