package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShopDataBeans;
import dao.ShopDao;

/**
 * Servlet implementation class yoyaku
 */
@WebServlet("/Yoyaku")
public class Yoyaku extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;

			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnStrUrl", "Index");
				// Login画面にリダイレクト
				response.sendRedirect("Login");
			} else {
			int id = Integer.parseInt(request.getParameter("item_id"));
			ShopDataBeans item = ShopDao.getItemByItemID(id);

			request.setAttribute("item", item);
			request.getRequestDispatcher(EcHelper.YOYAKU_PAGE).forward(request, response);
			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}
