package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PrefecturesDataBeans;
import beans.UserDataBeans;
import dao.PrefecturesDao;

/**
 * Servlet implementation class Userhistoryresult
 */
@WebServlet("/Userhistoryresult")
public class Userhistoryresult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Userhistoryresult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
				: false;

		if (!isLogin) {
			// Sessionにリターンページ情報を書き込む
			session.setAttribute("returnStrUrl", "Index");
			// Login画面にリダイレクト
			response.sendRedirect("Login");
		} else {
			UserDataBeans udb = (UserDataBeans) session.getAttribute("udb");
			ArrayList<PrefecturesDataBeans> pdList = PrefecturesDao.getAllPrefecturesDataBeans();
			String validationMessage = (String) EcHelper.cutSessionAttribute(session, "validationMessage");
			request.setAttribute("pdlist", pdList);
			request.setAttribute("udb", udb);
			request.setAttribute("validationMessage", validationMessage);
			request.getRequestDispatcher(EcHelper.USER_HISTORY_RESULT_PAGE).forward(request, response);

		}
	}
}
