package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.PrefecturesDataBeans;
import beans.ShopDataBeans;
import dao.PrefecturesDao;
import dao.ShopDao;

/**
 * Servlet implementation class ShophistoryDetail
 */
@WebServlet("/ShophistoryDetail")
@MultipartConfig(location = "/Users/kikuchishou/my-website/Project/WebContent/img", maxFileSize = 1048576)
public class ShophistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
					: false;

			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnStrUrl", "Index");
				// Login画面にリダイレクト
				response.sendRedirect("Login");
			} else {
			ArrayList<PrefecturesDataBeans> pdList = PrefecturesDao.getAllPrefecturesDataBeans();
			String id = request.getParameter("shop_id");
			int ShopId = Integer.parseInt(id);
			ShopDataBeans Shoplist = ShopDao.GetShopSearchId(ShopId);
			request.setAttribute("Shoplist", Shoplist);
			session.setAttribute("pdlist", pdList);
			request.getRequestDispatcher(EcHelper.SHOP_HISTORY_DETAIL_PAGE).forward(request, response);
			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		Part part = request.getPart("file");
		String name = this.getFileName(part);
		try {
			part.write(name);

			String Id = request.getParameter("shop_id");
			String inputUserName = request.getParameter("shop_name");
			String inputUserAddress = request.getParameter("shop_address");
			String inputUserPrefectures = request.getParameter("prefectures_id");
			String inputPrice = request.getParameter("shop_price");
			String inputDetail = request.getParameter("shop_detail");
			String inputUploadname = name;

			ShopDataBeans sdb = new ShopDataBeans();
			sdb.setName(inputUserName);
			sdb.setDetail(inputDetail);
			sdb.setAddress(inputUserAddress);
			sdb.setPrefectures_id(Integer.parseInt(inputUserPrefectures));
			sdb.setPrice(Integer.parseInt(inputPrice));
			sdb.setFileName(inputUploadname);
			sdb.setId(Integer.parseInt(Id));

			ShopDao.ShopDataUpdate(sdb);
			request.setAttribute("sdb", sdb);
			request.getRequestDispatcher(EcHelper.SHOP_HISTORY_RESULT_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}
}
