package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.PrefecturesDataBeans;
import beans.ShopDataBeans;
import dao.PrefecturesDao;
import dao.ShopDao;

@WebServlet("/ShopRegist")
@MultipartConfig(location = "/Users/kikuchishou/my-website/Project/WebContent/img", maxFileSize = 1048576)
public class ShopRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		//入力内容に誤りがある、確認画面で戻るボタン押し下げでアクセスしてきたときはセッションから入力情報を取得
		ShopDataBeans sdb = (ShopDataBeans) session.getAttribute("sdb") != null
				? (ShopDataBeans) EcHelper.cutSessionAttribute(session, "sdb")
				: new ShopDataBeans();
		ArrayList<PrefecturesDataBeans> pdList = PrefecturesDao.getAllPrefecturesDataBeans();
		String validationMessage = (String) EcHelper.cutSessionAttribute(session, "validationMessage");
		request.setAttribute("pdlist", pdList);
		request.setAttribute("sdb", sdb);
		request.setAttribute("validationMessage", validationMessage);
		request.getRequestDispatcher(EcHelper.SHOP_REGIST_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		Part part = request.getPart("file");
		String name = this.getFileName(part);
		try {
		part.write(name);

		String inputUserName = request.getParameter("shop_name");
		String inputUserAddress = request.getParameter("shop_address");
		String inputUserPrefectures = request.getParameter("prefectures_id");
		String inputPrice = request.getParameter("shop_price");
		String inputDetail = request.getParameter("shop_detail");
		String inputUploadname = name;

		ShopDataBeans sdb = new ShopDataBeans();
		sdb.setName(inputUserName);
		sdb.setDetail(inputDetail);
		sdb.setAddress(inputUserAddress);
		sdb.setPrefectures_id(Integer.parseInt(inputUserPrefectures));
		sdb.setPrice(Integer.parseInt(inputPrice));
		sdb.setFileName(inputUploadname);

			ShopDao.insertShop(sdb);
			request.setAttribute("sdb", sdb);
			request.getRequestDispatcher(EcHelper.SHOP_REGIST_RESULT_PAGE).forward(request, response);


	}catch(Exception e){
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
}


	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}
}
