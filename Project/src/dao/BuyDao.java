package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyDataBeans;

public class BuyDao {
	public static int insertBuy(BuyDataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy(user_id,price,yoyaku_date,create_date,usercomment,yoyaku_time) VALUES(?,?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getPrice());
			st.setString(3,bdb.getYoyaku_date());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.setString(5, bdb.getUsercomment());
			st.setString(6,bdb.getYoyaku_time());
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入情報登録処理
	 * @param bdb 購入情報
	 * @throws SQLException 呼び出し元にスローさせるため
	 */
public static BuyDataBeans getBuyDataBeansByBuyId(int buyId) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement(
				"SELECT * FROM t_buy WHERE t_buy.id = ?");
		st.setInt(1, buyId);

		ResultSet rs = st.executeQuery();

		BuyDataBeans bdb = new BuyDataBeans();
		if (rs.next()) {
			bdb.setId(rs.getInt("id"));
			bdb.setPrice(rs.getInt("price"));
			bdb.setBuyDate(rs.getTimestamp("create_date"));
			bdb.setUserId(rs.getInt("user_id"));
			bdb.setYoyaku_date(rs.getString("yoyaku_date"));
			bdb.setYoyaku_time(rs.getString("yoyaku_time"));
			bdb.setUsercomment(rs.getString("usercomment"));

		}


		return bdb;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}

public static List<BuyDataBeans> UserBuyHistoryDetail(int userId) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	List<BuyDataBeans> buylist = new ArrayList<>();
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement(
				"SELECT * FROM t_buy"
						+ " WHERE t_buy.user_id = ? "
						+ " ORDER BY t_buy.id desc");
		st.setInt(1, userId);
		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			BuyDataBeans bdb = new BuyDataBeans();
			bdb.setId(rs.getInt("id"));
			bdb.setPrice(rs.getInt("price"));
			bdb.setYoyaku_date(rs.getString("yoyaku_date"));
			bdb.setYoyaku_time(rs.getString("yoyaku_time"));
			bdb.setBuyDate(rs.getTimestamp("create_date"));
			bdb.setUserId(rs.getInt("user_id"));
			buylist.add(bdb);
		}
		return buylist;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
			}
		}
	}
}
