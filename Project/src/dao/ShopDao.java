package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ShopDataBeans;

public class ShopDao {
	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<ShopDataBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_shop ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<ShopDataBeans> itemList = new ArrayList<ShopDataBeans>();

			while (rs.next()) {
				ShopDataBeans item = new ShopDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ShopDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_shop WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ShopDataBeans item = new ShopDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setAddress(rs.getString("address"));
				item.setPrefectures_id(rs.getInt("Prefectures_id"));
			}
			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 * @param searchWord
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @param
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ShopDataBeans> getShopByItemName(String searchWord, int PrefecturesId ,int pageNum, int pageMaxItemCount)
			throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM m_shop WHERE Prefectures_id =?  ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1,PrefecturesId);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);



			} else {
				// 都道府県&ワード検索
				st = con.prepareStatement("SELECT * FROM m_shop WHERE name like ? and Prefectures_id =?  ORDER BY id ASC LIMIT ?,? ");
				st.setString(1, "%" + searchWord + "%");
				st.setInt(2,PrefecturesId);
				st.setInt(3, startiItemNum);
				st.setInt(4, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ShopDataBeans> itemList = new ArrayList<ShopDataBeans>();

			while (rs.next()) {
				ShopDataBeans item = new ShopDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setAddress(rs.getString("Address"));
				item.setFileName(rs.getString("file_name"));
				item.setPrefectures_id(rs.getInt("Prefectures_id"));
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getShopCount(String searchWord, int PrefecturesId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_shop where name like ? and Prefectures_id = ?");
			st.setString(1, "%" + searchWord + "%");
			st.setInt(2,PrefecturesId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}




	public static void insertShop(ShopDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO m_shop(name,detail,price,file_name,address,Prefectures_id,create_date) VALUES(?,?,?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getDetail());
			st.setInt(3, udb.getPrice());
			st.setString(4, udb.getFileName());
			st.setString(5, udb.getAddress());
			st.setInt(6, udb.getPrefectures_id());
			st.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<ShopDataBeans> GetfindAll() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<ShopDataBeans> shoplist = new ArrayList<>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM m_shop");
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				ShopDataBeans item = new ShopDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setAddress(rs.getString("address"));
				item.setFileName(rs.getString("file_name"));
				item.setBuyDate(rs.getTimestamp("create_date"));
				shoplist.add(item);
			}

			return shoplist;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<ShopDataBeans> GetShopSearch(int shopId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<ShopDataBeans> Shoplist = new ArrayList<ShopDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_shop WHERE m_shop.id = ?");
			st.setInt(1, shopId);

			ResultSet rs = st.executeQuery();
			ShopDataBeans sdb = new ShopDataBeans();
			while (rs.next()) {

				sdb.setId(rs.getInt("id"));
				sdb.setName(rs.getString("name"));
				sdb.setDetail(rs.getString("detail"));
				sdb.setPrice(rs.getInt("price"));
				sdb.setAddress(rs.getString("Address"));
				sdb.setFileName(rs.getString("file_name"));
				sdb.setBuyDate(rs.getTimestamp("create_date"));
				Shoplist.add(sdb);
			}

			return Shoplist;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ShopDataBeans GetShopSearchId(int shopId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_shop WHERE m_shop.id = ?");
			st.setInt(1, shopId);

			ResultSet rs = st.executeQuery();
			ShopDataBeans sdb = new ShopDataBeans();
			while (rs.next()) {

				sdb.setId(rs.getInt("id"));
				sdb.setName(rs.getString("name"));
				sdb.setDetail(rs.getString("detail"));
				sdb.setPrice(rs.getInt("price"));
				sdb.setAddress(rs.getString("Address"));
				sdb.setFileName(rs.getString("file_name"));
				sdb.setBuyDate(rs.getTimestamp("create_date"));

			}

			return sdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void ShopDataUpdate(ShopDataBeans sdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String SQL = "UPDATE m_shop SET name=? ,detail=?,price=?,file_name=?,address=?,Prefectures_id=? where id=?";
			st = con.prepareStatement(SQL);
			st.setString(1, sdb.getName());
			st.setString(2, sdb.getDetail());
			st.setInt(3, sdb.getPrice());
			st.setString(4, sdb.getFileName());
			st.setString(5, sdb.getAddress());
			st.setInt(6, sdb.getPrefectures_id());
			st.setInt(7, sdb.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void DeleteShopData(int id) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String delSQL = "DELETE FROM m_shop WHERE id = ? ";
			stmt = con.prepareStatement(delSQL);
			//stmt.setString(1, id);
			stmt.setInt(1, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}