package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDataBeans;
import controller.EcHelper;

public class UserDao {

	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_user(name,address,Prefectures_id,login_id,login_password,create_date) VALUES(?,?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getAddress());
			st.setInt(3, udb.getPrefectures_id());
			st.setString(4, udb.getLoginId());
			st.setString(5, EcHelper.getSha256(udb.getPassword()));
			st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				if (EcHelper.getSha256(password).equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");

					break;
				}
			}
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */
	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException {
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT id,name, login_id, address,Prefectures_id FROM t_user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setAddress(rs.getString("address"));
				udb.setPrefectures_id(rs.getInt("Prefectures_id"));

			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return udb;
	}

	/**
	 * ユーザー情報の更新処理を行う。
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void updateUser(UserDataBeans udb) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
		UserDataBeans updatedUdb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET name=?, login_id=?, address=?, Prefectures_id=? WHERE id=?;");
			st.setString(1, udb.getName());
			st.setString(2, udb.getLoginId());
			st.setString(3, udb.getAddress());
			st.setInt(4, udb.getPrefectures_id());
			st.setInt(5, udb.getId());
			st.executeUpdate();

			st = con.prepareStatement(
					"SELECT name, login_id, address, prefectures_id FROM t_user WHERE id=" + udb.getId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				updatedUdb.setName(rs.getString("name"));
				updatedUdb.setLoginId(rs.getString("login_id"));
				updatedUdb.setAddress(rs.getString("address"));
				updatedUdb.setPrefectures_id(rs.getInt("prefectures_id"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return isOverlap;
	}

	public static int Getprefectures_Id(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int prefectures_Id = 0;
			while (rs.next()) {
				if (EcHelper.getSha256(password).equals(rs.getString("login_password"))) {
					prefectures_Id = rs.getInt("prefectures_id");

					break;
				}
			}
			return prefectures_Id;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<UserDataBeans> GetUserfindAll() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<UserDataBeans> userlist = new ArrayList<>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_user");
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				UserDataBeans item = new UserDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setLoginId(rs.getString("login_id"));
				item.setPrefectures_id(rs.getInt("prefectures_id"));
				item.setAddress(rs.getString("address"));
				userlist.add(item);
			}

			return userlist;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<UserDataBeans> GetUserSearch(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<UserDataBeans> Userlist = new ArrayList<UserDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_user WHERE t_user.id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();
			UserDataBeans udb = new UserDataBeans();
			while (rs.next()) {

				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPrefectures_id(rs.getInt("prefectures_id"));
				udb.setAddress(rs.getString("address"));
				Userlist.add(udb);
			}
			return Userlist;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static UserDataBeans GetUserSearchId(int userId) throws SQLException {
		Connection con = null;

		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM t_user WHERE t_user.id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();
			UserDataBeans udb = new UserDataBeans();
			while (rs.next()) {

				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPrefectures_id(rs.getInt("prefectures_id"));
				udb.setAddress(rs.getString("address"));
			}
			return udb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void DeleteUserData(int id) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String delSQL = "DELETE FROM t_user WHERE id = ? ";
			stmt = con.prepareStatement(delSQL);
			//stmt.setString(1, id);
			stmt.setInt(1, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}