package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.PrefecturesDataBeans;

public class PrefecturesDao {

	public static ArrayList<PrefecturesDataBeans> getAllPrefecturesDataBeans() {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_Prefectures");

			ResultSet rs = st.executeQuery();

			ArrayList<PrefecturesDataBeans> PrefecturesDataList = new ArrayList<PrefecturesDataBeans>();
			while (rs.next()) {
				PrefecturesDataBeans pdb = new PrefecturesDataBeans();
				pdb.setId(rs.getInt("id"));
				pdb.setName(rs.getString("name"));
				PrefecturesDataList.add(pdb);
			}
			return PrefecturesDataList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static PrefecturesDataBeans getPrefecturesName(int PId) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_Prefectures_id WHERE =?");
			st.setInt(1, PId);

			ResultSet rs = st.executeQuery();

			PrefecturesDataBeans Pname = new PrefecturesDataBeans();
			while (rs.next()) {

				Pname.setId(rs.getInt("id"));
				Pname.setName(rs.getString("name"));
			}
			return Pname;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}