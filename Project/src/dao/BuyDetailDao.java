package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.BuyDetailDataBeans;
import beans.ShopDataBeans;

public class BuyDetailDao {
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/* public static ArrayList<ShopDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_shop.id,"
							+ " m_shop.name,"
							+ " m_shop.price"
							+ " FROM t_buy_detail"
							+ " JOIN m_shop"
							+ " ON t_buy_detail.item_id = m_shop.id"
							+ " WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ShopDataBeans> buyDetailItemList = new ArrayList<ShopDataBeans>();

			while (rs.next()) {
				ShopDataBeans idb = new ShopDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));

				buyDetailItemList.add(idb);
			}

			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();

			}

		}
	}
}*/
public static ShopDataBeans getItemDataBeansListByBuyId(int buyId) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement(
				"SELECT m_shop.id,"
						+ " m_shop.name,"
						+ " m_shop.price"
						+ " FROM t_buy_detail"
						+ " JOIN m_shop"
						+ " ON t_buy_detail.item_id = m_shop.id"
						+ " WHERE t_buy_detail.buy_id = ?");
		st.setInt(1, buyId);

		ResultSet rs = st.executeQuery();

		ShopDataBeans idb = new ShopDataBeans();
		while (rs.next()) {

			idb.setId(rs.getInt("id"));
			idb.setName(rs.getString("name"));
			idb.setPrice(rs.getInt("price"));


		}

		return idb;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();

		}

	}
}
}
