package base;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * DB接続処理全般
 */
public class DBManager {
    final private static String URL = "jdbc:mysql://localhost/";
    final private static String DB_NAME = "mywebsite";
    final private static String PARAMETERS = "?useUnicode=true&characterEncoding=utf8&allowPublicKeyRetrieval=true&useSSL=false";
    final private static String USER = "root";
    final private static String PASS = "password";

    /**
     * DBへ接続するコネクションを返す
     */
    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL+DB_NAME+PARAMETERS,USER,PASS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
}
