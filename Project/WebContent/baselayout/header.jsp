<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
		<nav class=" cyan accent-3" role="navigation">
			<div class="nav-wrapper container">
				<a href="Index" class="brand-logo">美容室検索サイト</a>
				<ul class="right">
				<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

				<%if(isLogin){ %>
					<li><a href="Userdata">ユーザー情報</a></li>
					<%}else{ %>
					<li><a href="Regist">新規登録</a></li>
					<%} %>

					<%if(isLogin){ %>
					<li><a href="Logout">ログアウト</a></li>
					<%}else{ %>
					<li><a href="Login">ログイン</a></li>
					<%} %>
				</ul>
			</div>
		</nav>