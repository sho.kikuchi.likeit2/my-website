<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>デリートページ</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">ショップデリート確認</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content"></div>
					<table class="bordered">
						<thead>
							<tr>
								<th class="center">値段</th>
								<th class="center">お店名</th>
							</tr>
						</thead>
						<tr>
							<td class="center">${sdb.price}</td>
							<td class="center">${sdb.name}</td>
						</tr>
						</tbody>
					</table>
					<div class="row">
						<div class="col s12">
							<p class="center-align">消去しますか？</p>
						</div>
						<div class="row">
							<div class="col s6 center-align">
								<button class="btn  waves-effect waves-light  col s6 offset-s3"
									type="button" name="confirm_Button" onclick="history.back();"
									value="戻る">戻る</button>
								<!--<div class="col s6 center-align">
									<button class="btn  waves-effect waves-light  col s6 offset-s3"
										type="submit" name="confirm_button" value="cancel">戻る</button>-->
							</div>
							<form action="ShopDeleteResult" method="post">
								<input name="shop_id" type="hidden" value="${sdb.id}" />
								<div class="col s6 center-align">
									<button class="btn  waves-effect waves-light  col s6 offset-s3"
										type="submit">消去</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>