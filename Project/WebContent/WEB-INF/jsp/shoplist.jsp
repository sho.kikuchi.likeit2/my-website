<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お店情報</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<br> <br>
		<div class="row center">
			<h5 class=" col s12 light">お店一覧</h5>
		</div>
		<div class="row center">
			<div class="input-field col s8 offset-s2">
				<form action="Shoplist" method="POST">
					<i class="material-icons prefix">search</i><input type="text"
						name="Shop_id"><label class= "active">お店をID検索</label>
					<button
						class="btn btn-large waves-effect waves-light  col s8 offset-s2"
						type="submit" name="action">検索</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="row-content">
						<table class="bordered">
							<thead>
								<tr>
									<th class="center">詳細</th>
									<th class="center">ショップID</th>
									<th class="center">ショップ名</th>
									<th class="center">住所</th>
									<th class="center">値段</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="shoplist" items="${shoplist}">
									<tr>
										<td class="center"><a
											href="ShophistoryDetail?shop_id=${shoplist.id}"
											class="btn-floating btn waves-effect waves-light "> <i
												class="material-icons">details</i></a></td>
										<td class="center">${shoplist.id}</td>
										<td class="center">${shoplist.name}</td>
										<td class="center">${shoplist.address}</td>
										<td class="center">${shoplist.price}円</td>
										<td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>