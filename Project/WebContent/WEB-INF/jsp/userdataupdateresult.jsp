<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<title>ユーザー更新リザルトページ</title>
<head>
<meta charset="UTF-8">
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="row center">
		<h5 class=" col s12 light">更新完了</h5>
	</div>
	<div class="row">
		<div class="section"></div>
		<div class="col s12">
			<div class="card grey lighten-5">
				<div class="card-content">
					<div class="row">
						<div class="input-field col s6">
							<input type="text" value="${udb.name}" readonly> <label>名前</label>
						</div>
						<div class="input-field col s6">
							<input type="text" value="${udb.loginId}" readonly> <label>ログインID</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<input type="text" value="${udb.address}" readonly> <label>住所</label>
						</div>
						<div class="row">
							<div class="input-field col s6">
								<select name="prefectures_id">
									<c:forEach var="pdl" items="${pdlist}">
										<c:if test="${pdl.id == udb.prefectures_id}">
											<option selected value="${pdl.id}">${pdl.name}</option>
										</c:if>
										<c:if test="${pdl.id != udb.prefectures_id}">
											<option disabled value="${pdl.id}">${pdl.name}</option>
										</c:if>
									</c:forEach>
								</select> <label>都道府県</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<p class="center-align">上記内容で更新しました</p>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<p class="center-align">
								<a href="Userdata"
									class="btn waves-effect waves-light  col s4 offset-s4">ユーザー情報へ</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>