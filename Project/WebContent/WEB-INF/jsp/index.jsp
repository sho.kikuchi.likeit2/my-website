<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>TOPページ</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br> <br>
			<h1 class="header center blue-text">美容室予約サイト</h1>
			<div class="row center">
				<h5 class="header col s12 light">見出し</h5>
			</div>
			<div class="row center">
				<div class="input-field col s8 offset-s2">
					<form action="ItemSearchResult">
						<i class="material-icons prefix">search</i> <input type="text"
							name="search_word">
						<div class=" row center">
							<div class="input-field col s8 offset-s2">
								<select name="prefectures_id">
									<c:forEach var="pdl" items="${pdlist}">
										<option value="${pdl.id}">${pdl.name}</option>
									</c:forEach>
								</select><label>都道府県検索</label>
								<button
									class="btn btn-large waves-effect waves-light  col s8 offset-s2"
									type="submit" name="action">検索</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>