<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ショップデータ登録</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">ショップ登録</h5>
			<c:if test="${validationMessage != null}">
				<P class="red-text">${validationMessage}</P>
			</c:if>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="#" method="POST" enctype="multipart/form-data">
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${sdb.name}" name="shop_name" type="text"
										required> <label>名前</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${sdb.address}" name="shop_address" type="text"
										required> <label>住所</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<select name="prefectures_id">
										<c:forEach var="pdl" items="${pdlist}">
											<option value="${pdl.id}">${pdl.name}</option>
										</c:forEach>
										</select><label>都道府県</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${sdb.price}" name="shop_price" type="text"
										required> <label>値段</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input name="file" type="file" required> <label class="active">ファイルアップロード</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${sdb.detail}" name="shop_detail" type="text" required>
									<label>詳細コメント</label>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<p class="center-align">
										<button
											class="btn btn-large waves-effect waves-light  col s8 offset-s2"
											type="submit" name="action">登録</button>
									</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>