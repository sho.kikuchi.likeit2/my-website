<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>予約確認ページ</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">予約内容を確認します</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row">
							<table class="bordered">
								<thead>
									<tr>
										<th class="center">予約日</th>
										<th class="center">予約時間</th>
										<th class="center">お店名</th>
										<th class="center">金額</th>
										<th class="center">一言</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th class="center">${bdb.yoyaku_date}</th>
										<td class="center">${bdb.yoyaku_time}</td>
										<td class="center">${item.name}</td>
										<td class="center">${bdb.price}円</td>
										<td class="center">${bdb.usercomment}</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col s6 center-align">
								<!--  <form action="YoyakuResult" method="post">
									<input name="item_id" type="hidden" value="${item.id}" />
									<input name="item_id" type="hidden" value="1" />
									<button class="btn  waves-effect waves-light  col s6 offset-s3"
										name="confirm_button" value="cancel" type="submit">予約修正</button>
											</form>-->
								<button class="btn  waves-effect waves-light  col s6 offset-s3"
									type="button" name="confirm_Button" onclick="history.back();"
									value="戻る">予約修正</button>
							</div>
							<div class="col s6  center-align">
								<form action="YoyakuResult" method="post">
									<input name="item_id" type="hidden" value="${item.id}" />
									<!--<input name="item_id" type="hidden" value="1" />
								<button class="btn  waves-effect waves-light  col s6 offset-s3"
									name="confirm_button" value="yoyaku" type="submit">予約確定</button>-->
									<button class="btn  waves-effect waves-light  col s6 offset-s3"
										type="submit">予約確定</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>