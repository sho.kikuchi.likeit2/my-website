<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>お店情報の詳細</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">ユーザー詳細</h4>
		</div>
		<div class="card-content">
			<div class="row">
				<div class="col s5"></div>
				<form action="UserDelete" method="post">
					<input name="user_id" type="hidden" value="${userlist.id}" />
					<div class="row">
						<div class="col s12">
							<p class="center-align">
								<button
									class="btn btn-large waves-effect waves-light  col s8 offset-s2"
									type="submit" name="action">ユーザー消去画面へ移動</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--  購入 -->
	<div class="row">
		<div class="col s6 offset-s3">
			<div class="card grey lighten-5">
				<div class="card-content">
					<table>
						<thead>
							<tr>
								<th class="center">ユーザーID</th>
								<th class="center">ログインID</th>
								<th class="center">名前</th>
								<th class="center">住所</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center">${userlist.id}</td>
								<td class="center">${userlist.loginId}</td>
								<td class="center">${userlist.name}</td>
								<td class="center">${userlist.address}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<form action="#" method="POST">
					<div class="card grey lighten-5">
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" name="user_name" value="${udb.name}" required>
								<label>名前</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" name="user_address" value="${udb.address}" required>
								<label>住所</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<select name="prefectures_id">
									<c:forEach var="pdl" items="${pdlist}">
										<option value="${pdl.id}">${pdl.name}</option>
									</c:forEach>
								</select><label>都道府県</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${udb.loginId}" name="login_id" type="text"
									required> <label>ログインID</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<button
										class="btn btn-large waves-effect waves-light  col s8 offset-s2"
										type="submit" name="action">更新</button>
								</p>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>