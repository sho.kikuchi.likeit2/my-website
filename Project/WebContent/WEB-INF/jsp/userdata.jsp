<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">ユーザー情報</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<form action="UserDataUpdateConfirm" method="POST">
					<div class="card grey lighten-5">
						<div class="card-content">
							<c:if test="${validationMessage != null}">
								<p class="red-text center-align">${validationMessage}</p>
							</c:if>
							<br> <br>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" name="user_name" value="${udb.name}">
									<label>名前</label>
								</div>
								<div class="input-field col s6">
									<input type="text" name="login_id" value="${udb.loginId}">
									<label>ログインID</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" name="user_address" value="${udb.address}">
									<label>住所</label>
								</div>
								<div class="input-field col s6">
									<select name="prefectures_id">
										<c:forEach var="pdl" items="${pdlist}">
											<c:if test="${pdl.id == udb.prefectures_id}">
												<option selected value="${pdl.id}">${pdl.name}</option>
											</c:if>
											<c:if test="${pdl.id != udb.prefectures_id}">
												<option value="${pdl.id}">${pdl.name}</option>
											</c:if>
										</c:forEach>
									</select> <label>都道府県</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<button class="btn  waves-effect waves-light  col s4 offset-s4"
								type="submit" name="action">更新</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="card-content">
					<div class="row">
						<div class="col s5"></div>
						<c:if test="${udb.loginId.equals('admin')||udbloginID.equals('omise')}">
							<a href="ShopRegist">お店登録画面へ移動</a>
						</c:if>
					</div>
					<div class="row">
						<div class="col s5"></div>
						<c:if test="${udb.loginId.equals('admin')||udbloginID.equals('omise')}">
							<a href="Shoplist">お店一覧画面へ移動</a>
						</c:if>
					</div>

					<div class="row">
						<div class="col s5"></div>
						<c:if test="${udb.loginId.equals('admin')}">
							<a href="UserList">ユーザー一覧画面へ移動</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  購入履歴 -->
	<div class="row">
		<div class="col s12">
			<div class="card grey lighten-5">
				<div class="row-content">
					<table class="bordered">
						<thead>
							<tr>
								<th class="center">詳細</th>
								<th class="center">予約日</th>
								<th class="center">予約時間</th>
								<th class="center">金額</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="buylist" items="${buylist}">
								<tr>
									<td class="center"><a
										href="UseryoyakuHistoryDetail?buy_id=${buylist.id}"
										class="btn-floating btn waves-effect waves-light "> <i
											class="material-icons">details</i></a></td>
									<td class="center">${buylist.yoyaku_date}</td>
									<td class="center">${buylist.yoyaku_time}</td>
									<td class="center">${buylist.price}円</td>
									<td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>