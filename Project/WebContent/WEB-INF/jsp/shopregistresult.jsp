<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>ショップデータ登録</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">ショップ登録確認</h5>
			<c:if test="${validationMessage != null}">
				<P class="red-text">${validationMessage}</P>
			</c:if>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.name}" name="shop_name" type="text" readonly>
								<label>名前</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.address}" name="shop_address" type="text"
									readonly> <label>住所</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<select name="prefectures_id">
									<c:forEach var="pdl" items="${pdlist}">
										<c:if test="${pdl.id == sdb.prefectures_id}">
											<option selected value="${pdl.id}">${pdl.name}</option>
										</c:if>
										<c:if test="${pdl.id != sdb.prefectures_id}">
											<option disabled value="${pdl.id}">${pdl.name}</option>
										</c:if>
									</c:forEach>
								</select> <label>都道府県</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.price}" name="shop_price" type="text">
								<label>値段</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.fileName}"  name="file" type="text" readonly> <label
									class="active">ファイル名</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.detail}" name="shop_detail" type="text"
									readonly> <label>詳細コメント</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">上記内容で登録しました。</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="Userdata"
										class="btn waves-effect waves-light  col s8 offset-s2">ユーザー画面へ戻る</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>