<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>予約購入ページ</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">予約</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="YoyakuConfirm" method="post">
							<input name="item_id" type="hidden" value="${item.id}" />
							<div class="row">
								<table class="bordered">
									<thead>
										<tr>
											<th class="center">お店名</th>
											<th class="center">基本料金</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="center">${item.name}</td>
											<td class="center">${item.price}円</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input name="yoyaku.date" type="date" required> <label
										class="active" for="yoyakubi">予約日</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input name="yoyaku.time" type="time" required> <label
										class="active" for="yoyakuzikan">予約時間</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<i class="material-icons prefix">textsms</i> <input
										name="yoyaku.usercomment" type="text"> <label>お店への一言</label>
								</div>
							</div>
							<div class="row">
								<div class="col s6 center-align">
									<button class="btn  waves-effect waves-light  col s6 offset-s3"
										type="button" name="action" onclick="history.back();">戻る</button>
								</div>
								<div class="col s6 center-align">
									<button class="btn  waves-effect waves-light  col s6 offset-s3"
										type="submit" name="action">予約支払いへ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>