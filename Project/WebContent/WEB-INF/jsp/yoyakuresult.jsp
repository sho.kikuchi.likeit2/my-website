<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>予約リザルトページ</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 ">予約が完了しました</h5>
		</div>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="col s6 center-align">
					<a href="index" class="btn  waves-effect waves-light ">引き続き買い物をする</a>
				</div>
				<div class="col s6 center-align">
					<a href="Userdata" class="btn  waves-effect waves-light">ユーザー情報へ</a>
				</div>
			</div>
		</div>
		<div class="row center">
			<h5 class=" col s12 light">予約詳細</h5>
		</div>
		<!--  購入 -->
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-4">
					<div class="card-content">
						<table>
							<thead>
								<tr>
										<th class="center">予約日</th>
										<th class="center">予約時間</th>
										<th class="center">お店名</th>
										<th class="center">金額</th>
										<th class="center">一言</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="center">${resultBDB.yoyaku_date}</td>
									<td class="center">${resultBDB.yoyaku_time}</td>
									<td class="center">${item.name}</td>
									<td class="center">${resultBDB.price}円</td>
									<td class="center">${resultBDB.usercomment}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</body>
</html>