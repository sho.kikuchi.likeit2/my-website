<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>予約情報の詳細</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">詳細</h4>
		</div>
	</div>
	<!--  購入 -->
	<div class="row">
		<div class="col s10 offset-s1">
			<div class="card grey lighten-5">
				<div class="card-content">
					<table>
						<thead>
							<tr>
								<th class="center">予約日</th>
								<th class="center">予約時間</th>
								<th class="center">お店名</th>
								<th class="center">金額</th>
								<th class="center">一言</th>
								<th class="center">予約制作日</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center">${buylist.yoyaku_date}</td>
								<td class="center">${buylist.yoyaku_time}</td>
								<td class="center">${buyI.name}</td>
								<td class="center">${buyI.price}円</td>
								<td class="center">${buylist.usercomment}</td>
								<td class="center">${buylist.formatDate}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>