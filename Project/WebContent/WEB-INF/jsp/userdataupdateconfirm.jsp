<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新ページ</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">入力内容確認</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="UserDataUpdateResult" method="post">
							<div class="row">
								<div class="input-field col s6 center">
									<input type="text" name="user_name_update" value="${udb.name}"
										readonly> <label>名前</label>
								</div>
								<div class="input-field col s6 center">
									<input type="text" name="login_id_update"
										value="${udb.loginId}" readonly> <label>ログインID</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6 center">
									<input type="text" name="user_address_update"
										value="${udb.address}" readonly> <label>住所</label>
								</div>
								<div class="input-field col s6 center">
									<select name="prefectures_id_update">
										<c:forEach var="pdl" items="${pdlist}">
											<c:if test="${pdl.id == udb.prefectures_id}">
												<option selected value="${pdl.id}">${pdl.name}</option>
											</c:if>
											<c:if test="${pdl.id != udb.prefectures_id}">
												<option disabled value="${pdl.id}">${pdl.name}</option>
											</c:if>
										</c:forEach>
									</select> <label>都道府県</label>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<p class="center-align">更新してよろしいでしょうか?</p>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="col s6 center-align">
										<button
											class="btn  waves-effect waves-light  col s6 offset-s3"
											type="submit" name="confirmButton" value="cancel">戻る</button>
									</div>
									<div class="col s6 center-align">
										<button
											class="btn  waves-effect waves-light  col s6 offset-s3"
											type="submit" name="confirmButton" value="update">更新</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>