<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>お店情報の詳細</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">お店詳細</h4>
		</div>
		<div class="card-content">
			<div class="row">
				<div class="col s5"></div>
				<form action="ShopDelete" method="post">
					<input name="shop_id" type="hidden" value="${Shoplist.id}" />
					<div class="row">
						<div class="col s12">
							<p class="center-align">
								<button
									class="btn btn-large waves-effect waves-light  col s8 offset-s2"
									type="submit" name="action">お店を消去画面へ移動</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--  購入 -->
	<div class="row">
		<div class="col s6 offset-s3">
			<div class="card grey lighten-5">
				<div class="card-content">
					<table>
						<thead>
							<tr>
								<th class="center">お店名</th>
								<th class="center">金額</th>
								<th class="center">詳細</th>
								<th class="center">制作日</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center">${Shoplist.name}</td>
								<td class="center">${Shoplist.price}</td>
								<td class="center">${Shoplist.detail}</td>
								<td class="center">${Shoplist.formatDate}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="card grey lighten-5">
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" name="shop_name" value="${sdb.name}">
								<label>名前</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" name="shop_address" value="${sdb.address}">
								<label>住所</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<select name="prefectures_id">
									<c:forEach var="pdl" items="${pdlist}">
										<option value="${pdl.id}">${pdl.name}</option>
									</c:forEach>
								</select><label>都道府県</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.price}" name="shop_price" type="text"
									required> <label>値段</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input name="file" type="file" required> <label
									class="active">ファイルアップロード</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${sdb.detail}" name="shop_detail" type="text"
									required> <label>詳細コメント</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<button
										class="btn btn-large waves-effect waves-light  col s8 offset-s2"
										type="submit" name="action">更新</button>
								</p>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>